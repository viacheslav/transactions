package isolation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Viacheslav.
 */
public class NonRepeatableRead {

    public static final int ISOLATION_LEVEL = Connection.TRANSACTION_READ_COMMITTED;

    public static void main(String[] args) {

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection initialConnection = null;

        Thread transaction1 = new Thread() {

            public void run() {
                Connection conn = null;
                try {
                    Thread.sleep(2000);
                    conn = DriverManager.getConnection("jdbc:mysql://localhost/findit?user=root&password=root");
                    conn.setAutoCommit(false);
                    conn.setTransactionIsolation(ISOLATION_LEVEL);

                    PreparedStatement select = conn.prepareStatement("SELECT * FROM `simple_table` WHERE `id` = 1");

                    PreparedStatement update = conn.prepareStatement("UPDATE `simple_table` SET field1 = CONCAT(?, 'First') WHERE `id` = 1");

                    ResultSet rs = select.executeQuery();
                    String valueFromFirstTransaction = "";
                    while (rs.next()) {
                        valueFromFirstTransaction = rs.getString("field1");
                    }

                    update.setString(1, valueFromFirstTransaction);
                    update.executeUpdate();
                    conn.commit();
                    System.out.println("First transaction updated value.");

                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Thread transaction2 = new Thread() {

            public void run() {
                Connection conn1 = null;
                try {
                    conn1 = DriverManager.getConnection("jdbc:mysql://localhost/findit?user=root&password=root");
                    conn1.setAutoCommit(false);
                    conn1.setTransactionIsolation(ISOLATION_LEVEL);

                    PreparedStatement select1 = conn1.prepareStatement("SELECT * FROM `simple_table` WHERE `id` = 1");

                    ResultSet rs1 = select1.executeQuery();
                    String valueFromSecondTransaction = "";
                    while (rs1.next()) {
                        valueFromSecondTransaction = rs1.getString("field1");
                        System.out.println("Read from the second transaction before update of the first: " + valueFromSecondTransaction);
                    }

                    Thread.sleep(5000);

                    ResultSet rs2 = select1.executeQuery();

                    while (rs2.next()) {
                        valueFromSecondTransaction = rs2.getString("field1");
                        System.out.println("Read from the second transaction after commit of the first: " + valueFromSecondTransaction);
                    }

                    conn1.commit();

                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        try {
            // Obtain connections


            initialConnection = DriverManager.getConnection("jdbc:mysql://localhost/findit?user=root&password=root");

            // Clear table
            PreparedStatement truncateTable = initialConnection.prepareStatement("TRUNCATE TABLE `simple_table`");
            truncateTable.execute();

            PreparedStatement insertInitial = initialConnection.prepareStatement("INSERT INTO `simple_table` (field1) VALUES ('Initial')");
            insertInitial.executeUpdate();

            PreparedStatement initialSelect = initialConnection.prepareStatement("SELECT * FROM `simple_table` WHERE `id` = 1");

            ResultSet initialRs = initialSelect.executeQuery();
            System.out.println("Initial state of database: ");
            while (initialRs.next()) {
                System.out.print(initialRs.getInt("id"));
                System.out.print(" | ");
                System.out.println(initialRs.getString("field1"));
            }
            System.out.println("******************************");

            transaction1.start();
            transaction2.start();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                initialConnection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
