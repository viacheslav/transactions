package isolation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Viacheslav.
 */
public class PhantomRead {

    private final static int ISOLATION_LEVEL = Connection.TRANSACTION_READ_COMMITTED;

    public static void main(String[] args) {

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Thread transaction1 = new Thread() {

            public void run() {
                Connection conn = null;
                try {
                    conn = DriverManager.getConnection("jdbc:mysql://localhost/findit?user=root&password=root");
                    conn.setAutoCommit(false);

                    conn.setTransactionIsolation(ISOLATION_LEVEL);

                    PreparedStatement insert = conn.prepareStatement("INSERT INTO `simple_table` (field1) VALUES ('Another row')");

                    Thread.sleep(2000);

                    insert.executeUpdate();
                    conn.commit();
                    System.out.println("First transaction committed");

                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }

        };


        Thread transaction2 = new Thread() {

            public void run() {
                Connection conn1 = null;
                try {
                    conn1 = DriverManager.getConnection("jdbc:mysql://localhost/findit?user=root&password=root");
                    conn1.setAutoCommit(false);

                    conn1.setTransactionIsolation(ISOLATION_LEVEL);

                    PreparedStatement select1 = conn1.prepareStatement("SELECT * FROM `simple_table`");
                    PreparedStatement select2 = conn1.prepareStatement("SELECT * FROM `simple_table`");

                    ResultSet rs1 = select1.executeQuery();
                    System.out.println("Initial state of database for second transaction: ");
                    while (rs1.next()) {
                        System.out.print(rs1.getInt("id"));
                        System.out.print(" | ");
                        System.out.println(rs1.getString("field1"));
                    }
                    System.out.println("******************************");


                    Thread.sleep(5000);

                    System.out.println("State of DB after commit of first transaction");
                    ResultSet rs2 = select2.executeQuery();
                    while (rs2.next()) {
                        System.out.print(rs2.getInt("id"));
                        System.out.print(" | ");
                        System.out.println(rs2.getString("field1"));
                    }
                    System.out.println("******************************");

                    conn1.commit();
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        conn1.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }

        };


        Connection initialConnection = null;
        try {
            initialConnection = DriverManager.getConnection("jdbc:mysql://localhost/findit?user=root&password=root");

            // Clear table
            PreparedStatement truncateTable = initialConnection.prepareStatement("TRUNCATE TABLE `simple_table`");
            truncateTable.execute();

            PreparedStatement insertInitial = initialConnection.prepareStatement("INSERT INTO `simple_table` (field1) VALUES ('Initial')");
            insertInitial.executeUpdate();

            transaction1.start();
            transaction2.start();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                initialConnection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
