package isolation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Viacheslav.
 */
public class LostUpdate {

    private static final int ISOLATION_LEVEL = Connection.TRANSACTION_READ_UNCOMMITTED;

    public static void main(String[] args) {


        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection initialConnection = null;

        Thread transaction1 = new Thread() {
            public void run() {
                Connection conn = null;
                try {
                    conn = DriverManager.getConnection("jdbc:mysql://localhost/findit?user=root&password=root");
                    conn.setAutoCommit(false);

                    conn.setTransactionIsolation(ISOLATION_LEVEL);
                    PreparedStatement update = conn.prepareStatement("UPDATE `simple_table` SET field1 = CONCAT(field1, 'First') WHERE `id` = 1");
                    update.executeUpdate();

                    Thread.sleep(4000);
                    conn.commit();
                    System.out.println("First transaction was committed");

                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Thread transaction2 = new Thread() {

            public void run() {

                Connection conn1 = null;

                try {
                    conn1 = DriverManager.getConnection("jdbc:mysql://localhost/findit?user=root&password=root");
                    conn1.setAutoCommit(false);

                    conn1.setTransactionIsolation(ISOLATION_LEVEL);

                    PreparedStatement select1 = conn1.prepareStatement("SELECT * FROM `simple_table` WHERE `id` = 1");
                    PreparedStatement update1 = conn1.prepareStatement("UPDATE `simple_table` SET field1 = CONCAT(field1, 'Second') WHERE `id` = 1");


                    System.out.println("State of DB from second transaction after first update");
                    ResultSet rsAfterFirst = select1.executeQuery();
                    while (rsAfterFirst.next()) {
                        System.out.print(rsAfterFirst.getInt("id"));
                        System.out.print(" | ");
                        System.out.println(rsAfterFirst.getString("field1"));
                    }
                    System.out.println("******************************");

                    update1.executeUpdate();

                    conn1.commit();
                    System.out.println("Second transaction was committed.");

                } catch (SQLException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        conn1.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        try {
            // Obtain connection
            initialConnection = DriverManager.getConnection("jdbc:mysql://localhost/findit?user=root&password=root");

            // Clear table
            PreparedStatement truncateTable = initialConnection.prepareStatement("TRUNCATE TABLE `simple_table`");
            truncateTable.execute();

            PreparedStatement insertInitial = initialConnection.prepareStatement("INSERT INTO `simple_table` (field1) VALUES ('Initial')");
            insertInitial.executeUpdate();

            PreparedStatement initialSelect = initialConnection.prepareStatement("SELECT * FROM `simple_table` WHERE `id` = 1");

            String currentValue = "";

            ResultSet initialRs = initialSelect.executeQuery();
            System.out.println("Initial state of database: ");
            while (initialRs.next()) {
                currentValue = initialRs.getString("field1");

                System.out.print(initialRs.getInt("id"));
                System.out.print(" | ");
                System.out.println(currentValue);
            }
            System.out.println("******************************");

            transaction1.start();
            transaction2.start();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                initialConnection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
