package savepoints;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;

/**
 * Created by Viacheslav.
 */
public class SavepointExample {

    public static void main(String[] args) {

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection initialConnection = null;

        Thread transaction1 = new Thread() {

            public void run() {
                Connection conn = null;
                try {
                    conn = DriverManager.getConnection("jdbc:mysql://localhost/findit?user=root&password=root");
                    conn.setAutoCommit(false);

                    PreparedStatement insert = conn.prepareStatement("INSERT INTO `simple_table` (`field1`) VALUES ('First')");
                    PreparedStatement insert2 = conn.prepareStatement("INSERT INTO `simple_table` (`field1`) VALUES ('Second')");

                    insert.executeUpdate();
                    Savepoint savepoint = conn.setSavepoint("Savepoint");

                    insert2.executeUpdate();

                    conn.rollback(savepoint);
                    conn.commit();
                    System.out.println("First transaction was rollbacked to savepoint and committed.");

                } catch (SQLException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

            }
        };


        try {
            // Obtain connections


            initialConnection = DriverManager.getConnection("jdbc:mysql://localhost/findit?user=root&password=root");

            // Clear table
            PreparedStatement truncateTable = initialConnection.prepareStatement("TRUNCATE TABLE `simple_table`");
            truncateTable.execute();

            PreparedStatement insertInitial = initialConnection.prepareStatement("INSERT INTO `simple_table` (field1) VALUES ('Initial')");
            insertInitial.executeUpdate();

            PreparedStatement initialSelect = initialConnection.prepareStatement("SELECT * FROM `simple_table` WHERE `id` = 1");

            ResultSet initialRs = initialSelect.executeQuery();
            System.out.println("Initial state of database: ");
            while (initialRs.next()) {
                System.out.print(initialRs.getInt("id"));
                System.out.print(" | ");
                System.out.println(initialRs.getString("field1"));
            }
            System.out.println("******************************");

            transaction1.start();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                initialConnection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
