package jpa;

import jpa.entities.SimpleTable;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.LockModeType;
import javax.persistence.Persistence;


/**
 * Created by Viacheslav.
 */
public class JpaLocks {

    public static void main(String[] args) {

        Thread transaction1 = new Thread() {

            public void run() {
                EntityManagerFactory emf = Persistence.createEntityManagerFactory("findit");

                EntityManager em = emf.createEntityManager();

                em.getTransaction().begin();

                SimpleTable st = em.find(SimpleTable.class, 1, LockModeType.OPTIMISTIC);
                System.out.println("First transaction: field is : " + st.getField());

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                em.getTransaction().rollback();
                System.out.println("First transaction was rollbacked.");

                em.close();
                emf.close();
            }
        };

        Thread transaction2 = new Thread() {

            public void run() {

                EntityManagerFactory emf = Persistence.createEntityManagerFactory("findit");

                EntityManager em = emf.createEntityManager();

                em.getTransaction().begin();

                SimpleTable st = em.find(SimpleTable.class, 1, LockModeType.OPTIMISTIC);
                System.out.println("Second transaction: field is : " + st.getField());

                st.setField("Second" + st.getField());
                em.merge(st);

                em.getTransaction().commit();
                System.out.println("Second transaction was committed.");

                em.close();
                emf.close();

            }
        };

        try {
            transaction1.start();
            Thread.sleep(1000);
            transaction2.start();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
