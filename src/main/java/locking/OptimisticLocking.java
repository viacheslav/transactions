package locking;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Viacheslav.
 */
public class OptimisticLocking {

    public static void main(String[] args) {

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection initialConnection = null;

        Thread transaction1 = new Thread() {

            public void run() {
                Connection conn = null;
                try {
                    conn = DriverManager.getConnection("jdbc:mysql://localhost/findit?user=root&password=root");
                    conn.setAutoCommit(false);
                    conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

                    PreparedStatement select = conn.prepareStatement("SELECT * FROM `simple_table` WHERE `id` = 1");
                    PreparedStatement update = conn.prepareStatement("UPDATE `simple_table` SET `field1` = 'First', `version` = `version` + 1 WHERE `id` = 1");

                    System.out.println("State of DB for first transaction");
                    ResultSet rs = select.executeQuery();
                    int version = 0;
                    while (rs.next()) {
                        System.out.print(rs.getInt("id"));
                        System.out.print(" | ");
                        System.out.print(rs.getString("field1"));
                        System.out.print(" | ");
                        version = rs.getInt("version");
                        System.out.println(version);
                    }
                    System.out.println("******************************");

                    Thread.sleep(5000);

                    update.executeUpdate();
                    ResultSet rs1 = select.executeQuery();
                    rs1.next();
                    int currentVersion = rs1.getInt("version");
                    if ((version + 1) == currentVersion) {
                        conn.commit();
                        System.out.println("Committed first transaction, version is " + currentVersion);
                    } else {
                        conn.rollback();
                        System.out.println("Rollbacked first transaction, version is " + currentVersion);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

            }
        };

        Thread transaction2 = new Thread() {

            public void run() {
                Connection conn1 = null;
                try {

                    conn1 = DriverManager.getConnection("jdbc:mysql://localhost/findit?user=root&password=root");

                    conn1.setAutoCommit(false);
                    conn1.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);

                    PreparedStatement select1 = conn1.prepareStatement("SELECT * FROM `simple_table` WHERE `id` = 1");
                    PreparedStatement update1 = conn1.prepareStatement("UPDATE `simple_table` SET `field1` = 'Second', `version` = `version` + 1 WHERE `id` = 1");

                    update1.executeUpdate();

                    conn1.commit();
                    System.out.println("Second transaction committed.");

                } catch (SQLException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        conn1.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

            }
        };

        try {
            // Obtain connections


            initialConnection = DriverManager.getConnection("jdbc:mysql://localhost/findit?user=root&password=root");

            // Clear table
            PreparedStatement truncateTable = initialConnection.prepareStatement("TRUNCATE TABLE `simple_table`");
            truncateTable.execute();

            PreparedStatement insertInitial = initialConnection.prepareStatement("INSERT INTO `simple_table` (field1, version) VALUES ('Initial', 0)");
            insertInitial.executeUpdate();

            PreparedStatement initialSelect = initialConnection.prepareStatement("SELECT * FROM `simple_table` WHERE `id` = 1");

            ResultSet initialRs = initialSelect.executeQuery();
            System.out.println("Initial state of database: ");
            while (initialRs.next()) {
                System.out.print(initialRs.getInt("id"));
                System.out.print(" | ");
                System.out.print(initialRs.getString("field1"));
                System.out.print(" | ");
                System.out.println(initialRs.getInt("version"));
            }
            System.out.println("******************************");

            transaction1.start();
            Thread.sleep(1000);
            transaction2.start();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                initialConnection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

}
