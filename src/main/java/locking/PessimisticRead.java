package locking;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Viacheslav.
 */
public class PessimisticRead {

    public static void main(String[] args) {

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection initialConnection = null;

        Thread transaction1 = new Thread() {

            public void run() {
                Connection conn = null;
                try {
                    conn = DriverManager.getConnection("jdbc:mysql://localhost/findit?user=root&password=root");
                    conn.setAutoCommit(false);

                    PreparedStatement select = conn.prepareStatement("SELECT * FROM `simple_table` WHERE `id` = 1 LOCK IN SHARE MODE");

                    System.out.println("State of DB for first transaction");
                    ResultSet rs = select.executeQuery();
                    while (rs.next()) {
                        System.out.print(rs.getInt("id"));
                        System.out.print(" | ");
                        System.out.println(rs.getString("field1"));
                    }
                    System.out.println("******************************");

                    Thread.sleep(5000);

                    conn.rollback();
                    System.out.println("First transaction was rollbacked.");

                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

            }
        };

        Thread transaction2 = new Thread() {

            public void run() {
                Connection conn1 = null;
                try {

                    conn1 = DriverManager.getConnection("jdbc:mysql://localhost/findit?user=root&password=root");

                    conn1.setAutoCommit(false);

                    PreparedStatement select1 = conn1.prepareStatement("SELECT * FROM `simple_table` WHERE `id` = 1");
                    PreparedStatement update1 = conn1.prepareStatement("UPDATE `simple_table` SET `field1` = 'SECOND' WHERE `id` = 1");


                    String currentValue = "";
                    System.out.println("State of DB for second transaction");
                    ResultSet rs1 = select1.executeQuery();
                    while (rs1.next()) {
                        currentValue = rs1.getString("field1");

                        System.out.print(rs1.getInt("id"));
                        System.out.print(" | ");
                        System.out.println(currentValue);
                    }
                    System.out.println("******************************");

                    update1.executeUpdate();
                    System.out.println("Executed update in second transaction.");

                    conn1.commit();
                    System.out.println("Second transaction was committed.");

                } catch (SQLException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        conn1.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

            }
        };

        try {
            // Obtain connections


            initialConnection = DriverManager.getConnection("jdbc:mysql://localhost/findit?user=root&password=root");

            // Clear table
            PreparedStatement truncateTable = initialConnection.prepareStatement("TRUNCATE TABLE `simple_table`");
            truncateTable.execute();

            PreparedStatement insertInitial = initialConnection.prepareStatement("INSERT INTO `simple_table` (field1) VALUES ('Initial')");
            insertInitial.executeUpdate();

            PreparedStatement initialSelect = initialConnection.prepareStatement("SELECT * FROM `simple_table` WHERE `id` = 1");

            ResultSet initialRs = initialSelect.executeQuery();
            System.out.println("Initial state of database: ");
            while (initialRs.next()) {
                System.out.print(initialRs.getInt("id"));
                System.out.print(" | ");
                System.out.println(initialRs.getString("field1"));
            }
            System.out.println("******************************");

            transaction1.start();
            Thread.sleep(1000);
            transaction2.start();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                initialConnection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
